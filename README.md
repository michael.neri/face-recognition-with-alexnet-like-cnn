Python code which permits to 

- Train a Alexnet-Like CNN model for detection and recognition of VIPs images (13);
- Import the trained model into the python script finalRecognition.py;
- Detect faces from photos in input and return which person is with its accuracy.


How to use the program and further considerations of the project are inside the report for the Digital Forensics course A.A. 19/20 @ UniPD.


If there are some questions about implementations michael.neri@studenti.unipd.it